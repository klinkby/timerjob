﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace Klinkby.TimerJob
{
    public sealed class FileJobPersister : IJobPersister
    {
        private const int StreamBufferSize = 1024;
        private static readonly Encoding Encoding = Encoding.UTF8; 
        private readonly FileStream _fileStream;
        private DateTime _lastRun;

        public FileJobPersister(string filePath, DateTime defaultStartTime)
        {
            if (filePath == null) throw new ArgumentNullException("filePath");
            _fileStream = new FileStream(
                filePath,
                FileMode.OpenOrCreate,
                FileAccess.ReadWrite,
                FileShare.Read);
            try
            {
                _lastRun = LoadLastRun();
            }
            catch (Exception)
            {
                _lastRun = defaultStartTime;
            }
        }

        public DateTime LastRun
        {
            get
            {
                return _lastRun;
            }
            set
            {
                SaveLastRun(_lastRun = value);
            }
        }

        public void Dispose()
        {
            _fileStream.Dispose();
        }

        private DateTime LoadLastRun()
        {
            string lastLine = null;
            using (var rd = new StreamReader(
                _fileStream, 
                Encoding, 
                detectEncodingFromByteOrderMarks: false,
                bufferSize: StreamBufferSize, 
                leaveOpen: true))
            {
                string line;
                while ((line = rd.ReadLine()) != null)
                {
                    lastLine = line;
                }
                // cursor now at end of file
            }
            var dt = DateTime.Parse(lastLine, CultureInfo.InvariantCulture).ToUniversalTime();
            return dt;
        }

        private void SaveLastRun(DateTime dateTime)
        {
            using (var wr = new StreamWriter(
                _fileStream,
                Encoding,
                StreamBufferSize,
                leaveOpen: true))
            {
                wr.WriteLine(dateTime.ToString("u", CultureInfo.InvariantCulture));
                wr.Flush();
            }
        }
    }
}