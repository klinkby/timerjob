﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;

namespace Klinkby.TimerJob.Example
{
    internal static class Program
    {
        private const string Job1Filename = "job1.log";
        private const string Job2Filename = "job2.log";
        private static readonly TimeSpan Resolution = TimeSpan.FromMinutes(1);
        private static readonly DateTime FirstRun = default(DateTime);

        private static void Main(string[] args)
        {
            var jobs = new ConcurrentQueue<IJob>(CreateInitialTasks());
            using (var mgr = new JobManager(jobs, Resolution))
            {
                // it's running now, but hey... let's add another task
                jobs.Enqueue(CreateAnotherTask());
                mgr.Error += Worker_Error; // log any error
                Console.WriteLine("Hit enter to stop");
                Console.ReadLine();
            }
        }

        // this job prints time every second minute
        private static IEnumerable<IJob> CreateInitialTasks()
        {
            string logFilePath = Path.Combine(Environment.CurrentDirectory, Job1Filename);
            var logPersister = new FileJobPersister(logFilePath, FirstRun);
            yield return new Job(
                WriteTimeCommand,
                Periodicity.Minute,
                2,
                logPersister);
        }

        // this job greets every hour
        private static IJob CreateAnotherTask()
        {
            string logFilePath = Path.Combine(Environment.CurrentDirectory, Job2Filename);
            var logPersister = new FileJobPersister(logFilePath, FirstRun);
            return new Job(
                WriteGreetingCommand,
                Periodicity.Hour,
                1,
                logPersister);
        }

        private static void WriteTimeCommand()
        {
            Console.WriteLine("Time is @ " + DateTime.UtcNow);
        }

        private static void WriteGreetingCommand()
        {
            Console.WriteLine("Hello. Did you wait long for this?");
        }

        private static void Worker_Error(object sender, ErrorEventArgs errorEventArgs)
        {
            Console.WriteLine(errorEventArgs.GetException().ToString());
        }
    }
}